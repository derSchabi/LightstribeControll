

const int redPin = 11;
const int redMax = 70;
const int redDeltaVal = 1;
int redDelta = 1;
int redValue = 0;

const int bluePin = 10;
const int blueMax = 70;
const int blueDeltaVal = 1;
int blueDelta = 0;
int blueValue = 0;

const int greenPin = 9;
const int greenMax = 70;
const int greenDeltaVal = 1;
int greenDelta = 0;
int greenValue = 0;


// global states changable by shell or etc
int sleep = 500;
int red;
int green;
int blue;

/* --------------------
 *  States set by shell
 *  ------------------
 */

enum Mode {
  rainbow,
  off,
  single_color
} mode;
//------------------

void setColor(int r, int g, int b) {
  if(r > redMax) r = redMax;
  if(g > greenMax) r = greenMax;
  if(b > blueMax) r = blueMax;
  if(r < 0) r = 0;
  if(g < 0) g = 0;
  if(b < 0) b = 0;
  red = r;
  green = g;
  blue = b;
  analogWrite(redPin, red);
  analogWrite(greenPin, green);
  analogWrite(bluePin, blue);
}

String command = "";

// shell will also create a delay
void shell() {
  if(Serial.available() == 0)
    return;
  char input = Serial.read();
  Serial.write(input);
  if(input != 13) {
    command += input;
    return;
  }
  Serial.println("");
  
  if(command.equals("help")) {
    Serial.println(
      "Arduino light controll by Schabi\r\n"
      "Possible commands:\r\n"
      " help     shows this help\r\n"
      " rainbow  make an RGB color fade\r\n"
      " off      turns off the lightstribe\r\n"
      " delay    usage \"delay [002-999]\" in ms\r\n"
      " color    set color"
      );
    Serial.print(
      "          usage:\t\n"
      "            \"color [r] [g] [b]\"\r\n"
      "          use values from 00 - ");
    Serial.println(redMax);
    Serial.println(
      " get      usage \"get [color, delay]\""
      );
  } else if(command.startsWith("delay")) {
    sleep = command.substring(6, 9).toInt();
    if(sleep < 2) {
      sleep = 2;
    }
    Serial.setTimeout(sleep);
  } else if(command.equals("rainbow")) {
    mode = rainbow;
  }else if(command.equals("off")) {
    mode = off;
    setColor(0, 0, 0);
  }else if(command.startsWith("get")) {
    if(command.endsWith(" color")) {
      Serial.print(red);
      Serial.print(" ");
      Serial.print(green);
      Serial.print(" ");
      Serial.println(blue);
    }else if(command.endsWith(" delay")) {
      Serial.println(sleep);
    } else {
      Serial.println("wrong, type help for more info");
    }
  }else if(command.startsWith("color")) {
    mode = single_color;
    int r = command.substring(6, 8).toInt();
    int g = command.substring(9, 11).toInt();
    int b = command.substring(12, 14).toInt();
    setColor(r, g, b);
  } else {  
    Serial.println("command not found");
  }

  Serial.print("$ ");
  command = "";
}

// make an RGB color fade
void rainBowColor() {
  blueValue += blueDelta;
  redValue += redDelta;
  greenValue += greenDelta;

  // change state algorithm
  if(blueValue == blueMax) {
    blueValue = blueMax;
    blueDelta = -blueDeltaVal;
    redDelta = redDeltaVal;
    greenDelta = 0;
  } else if(redValue == redMax) {
    redValue = redMax;
    blueDelta = 0;
    redDelta = -redDeltaVal;
    greenDelta = greenDeltaVal;
  } else if(greenValue == greenMax) {
    greenValue = greenMax;
    blueDelta = blueDeltaVal;
    redDelta = 0;
    greenDelta = -greenDeltaVal;
  }
  
  setColor(redValue, greenValue, blueValue);
}

void setup() {
  pinMode(bluePin, OUTPUT);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);

  // first put off everything since computer will not boot while leds are on
  analogWrite(bluePin, 0);
  analogWrite(redPin, 0);
  analogWrite(greenPin, 0);

  Serial.begin(9600);
  Serial.setTimeout(sleep);
}

// the loop function runs over and over again forever
void loop() {
  for(int i = 0; i < sleep; i++) {
    delay(1);
    shell();
  }
  switch(mode) {
    case rainbow:
      rainBowColor();
      break;
    case single_color:
      break;
    case off:
      break;
    default:
      Serial.println("mode not found");
  }
}
